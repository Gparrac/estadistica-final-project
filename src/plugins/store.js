import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    active: false,
    data: []
  },
  mutations: {
    setData(state, data) {
      state.data = data;
    },
    changeActive(state, status){
      state.active = status
    }
  }
})