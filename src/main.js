import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import store from './plugins/store'
import chart from './plugins/chart';

Vue.config.productionTip = false

new Vue({
  vuetify,
  store,
  chart,
  render: h => h(App)
}).$mount('#app')
