const { VueLoaderPlugin } = require('vue-loader');
module.exports = {
    entry: "./src/main.js",
    output:{
        path: __dirname + '/dist',
        filename: "build.js",
    },
    module:{
        rules: [
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    {
                        loader: ['sass-loader', 'css-loader'],
                        options: {
                            modules: true,
                            localIdentName: '[local]_[hash:base64:8]',
                            implementation: require('sass'),
                        }
                    }
                ]
            },
            {
                test: /\.vue$/,
                use: "vue-loader",
            },
            {
                test: /\.js$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: "file-loader",
                options:{
                    name: "[name].[ext]?[hash]"
                }
            }
        ]
    },
    plugins:[
        new VueLoaderPlugin()
    ],
    resolve: {
        alias:{
            vue$: "vue/dist/vue.esm.js"
        },
        extensions: ["*", ".js", ".vue", ".json"]
    },
};